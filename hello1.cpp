///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04b - Hello World
//
// File: hello1.cpp
//
// @author Melvin Alhambra <melvin42@hawaii.edu>
// @date   14/02/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){
   cout << "Hello World!" << endl;

return 0;

}

